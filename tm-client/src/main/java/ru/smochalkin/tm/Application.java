package ru.smochalkin.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.smochalkin.tm.component.Bootstrap;
import ru.smochalkin.tm.configuration.ContextConfiguration;

public class Application {

    public static void main(final String[] args) {
        @NotNull final AbstractApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        context.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
    }

}
