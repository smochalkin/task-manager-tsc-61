package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractProjectListener;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.system.AccessDeniedException;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all projects of current user.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        @NotNull final Result result = projectEndpoint.clearProjects(sessionService.getSession());
        printResult(result);
    }

}
