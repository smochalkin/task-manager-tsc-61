package ru.smochalkin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;
import ru.smochalkin.tm.listener.AbstractSystemListener;

import java.util.List;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String arg() {
        return "-arg";
    }

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of arguments.";
    }

    @NotNull
    @Autowired
    private AbstractSystemListener[] systemListeners;

    @Override
    @EventListener(condition = "@argumentListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : systemListeners) {
            System.out.println(listener);
        }
    }

}
