package ru.smochalkin.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractSystemListener;

@Component
public final class VersionListener extends AbstractSystemListener {

    @Override
    @NotNull
    public String arg() {
        return "-v";
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

    @Override
    @NotNull
    public String description() {
        return "Display version.";
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("version"));
    }

}
