package ru.smochalkin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractProjectListener;
import ru.smochalkin.tm.endpoint.ProjectDto;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Show project by index.";
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        @NotNull final ProjectDto project = projectEndpoint.findProjectByIndex(sessionService.getSession(), --index);
        showProject(project);
    }

}
